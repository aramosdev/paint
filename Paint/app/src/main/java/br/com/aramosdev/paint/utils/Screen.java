package br.com.aramosdev.paint.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by alberto on 21/04/15.
 */
public class Screen {
    private Bitmap bitmap;
    private Canvas canvas;
    private Paint paint;
    private float downX, downY, upX, upY, displayHeight, displayWidth;

    public Screen(float displayHeight, float displayWidth) {
        this.displayHeight = displayHeight;
        this.displayWidth = displayWidth;
        this.bitmap = Bitmap.createBitmap((int)this.displayWidth,(int)this.displayHeight,Bitmap.Config.ARGB_8888);
    }
    public Bitmap createCanvas(int color){
        this.canvas = new Canvas(this.bitmap);
        this.canvas.drawColor(color);
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        this.paint.setDither(true);
        this.paint.setColor(Color.WHITE);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeJoin(Paint.Join.ROUND);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setTextSize(20);
        this.paint.setStrokeWidth(5);

        return this.bitmap;
    }
    public void generatorLine(int action, MotionEvent event){
        switch (action){
            case MotionEvent.ACTION_DOWN:
                this.downX = event.getRawX();
                this.downY = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                this.upX = event.getRawX();
                this.upY = event.getRawY();
                this.canvas.drawLine(this.downX, this.downY, this.upX, this.upY, this.paint);
                this.downX = this.upX;
                this.downY = this.upY;
                break;
            case MotionEvent.ACTION_UP :
                this.upY = event.getRawX();
                this.upX = event.getX();
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
        }
    }
    public void clear(){
        Paint clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this.canvas.drawRect(0, 0,  this.displayWidth,  this.displayHeight, clearPaint);
    }
    public String createFileImage(Bitmap bitmap,String nameImage) throws IOException {
        Log.d("teste", Screen.class.getPackage().getName());
        File dir = new File(Environment.getExternalStorageDirectory().getPath()+"/"+Screen.class.getPackage().getName()+"/");
        if (!dir.exists())  dir.mkdirs();
        File cachePath = new File(dir.getAbsolutePath()+"/"+nameImage+".jpg");
        cachePath.createNewFile();
        FileOutputStream outputStream = new FileOutputStream(cachePath);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.close();

        return dir.getAbsolutePath()+"/"+nameImage+".jpg";
    }
}
