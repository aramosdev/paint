package br.com.aramosdev.paint.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import br.com.aramosdev.paint.R;
import br.com.aramosdev.paint.utils.Screen;

/**
 * Created by alberto on 20/04/15.
 */
public class PaintActivity extends ActionBarActivity implements View.OnTouchListener {
    private Toolbar toolbar;
    private ImageView screenImage;
    private Bitmap bitmap;
    private Screen screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paint);
//        this.toolbar = (Toolbar) findViewById(R.id.app_bar);
//        setSupportActionBar(this.toolbar);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.screenImage = (ImageView) findViewById(R.id.screen);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        this.screen = new Screen(getResources().getDimension(R.dimen.dimens_screen),metrics.widthPixels );
        this.bitmap = this.screen.createCanvas(Color.BLACK);

        this.screenImage.setImageBitmap(this.bitmap);
        this.screenImage.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        this.screen.generatorLine(action,event);
        this.screenImage.invalidate();
        return true;
    }
}
