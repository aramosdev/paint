package br.com.aramosdev.paint.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import br.com.aramosdev.paint.R;

/**
 * Created by alberto on 20/04/15.
 */
public class HomeActivity extends ActionBarActivity {

    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        this.toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(this.toolbar);
    }
    public void openPaint(View v){
        Intent intent = new Intent(this, PaintActivity.class);
        startActivity(intent);
    }
}
